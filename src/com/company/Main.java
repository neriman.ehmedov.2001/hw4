package com.company;

public class Main {
    public static void main(String args[]) {
        Pet nerimansPet = new Pet("dog", "Alok", 2, 45, new String[]{"eat, play, sleep"});
        Human nerimansmother = new Human("Aynur", "Ahmadov", 1897);
        Human nerimansfather = new Human("Ayaz", "Ahmadov", 1891);
        Human neriman = new Human("Neriman", "Ahmadov", 2001, 160, nerimansfather, nerimansmother, nerimansPet,
                new String[][]{{"Sunday", "do home work"},
                        {"Monday", "go to courses; watch a film"}, {"Tuesday", "do sport"},
                        {"Wednesday", "read some books"}, {"Thursday", "do shopping"},
                        {"Friday", "do housework"},
                        {"Saturday ", "visit your relatives"}});
        nerimansPet.respond();
        nerimansPet.eat();
        nerimansPet.foul();
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println(nerimansPet.toString());
        neriman.describePet();
        neriman.greetPet();
        System.out.println(neriman.toString());

        System.out.println("=============================================================================");
        System.out.println("=============================================================================");


        Pet samirPet = new Pet("lion", "Kotodama", 4, 60, new String[]{"hunt, take a nap, hide"});
        Human motherE = new Human("Elizabeth", "Sukihiro", 1891);
        Human fatherY = new Human("Yami", "Sukihiro", 1891);
        Human samir = new Human("Samir", "Sukihiro", 1970, 63, fatherY, motherE, samirPet,
                new String[][]{{"Sunday", "do rest"},
                        {"Monday", "go to work; watch a anime"}, {"Tuesday", "go to course"},
                        {"Wednesday", "read application for oikos"},
                        {"Thursday", "do online shopping"}, {"Friday", "hang out "},
                        {"Saturday ", "talking grandparents"}});
        samirPet.respond();
        samirPet.eat();
        samirPet.foul();
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println(samirPet.toString());
        samir.describePet();
        samir.greetPet();
        System.out.println(samir.toString());

        System.out.println("=============================================================================");
        System.out.println("=============================================================================");


        Pet andresPet = new Pet("pokemon", "Lucario", 106, 100, new String[]{"casting, spell control, " + "create auro ball"});
        Human motherA = new Human("Alina", "FHjhjdhj", 1950);
        Human fatherS = new Human("Sasha", "FHjhjdhj", 1945);
        Human andres = new Human("Andres", "FHjhjdhj", 1980, 99, fatherS, motherA, andresPet,
                new String[][]{{"Sunday", "try some sport"}, {"Monday", "go to courses"},
                        {"Tuesday", "prepare materials "},
                        {"Wednesday", "read  fav manga"},
                        {"Thursday", "go to course"}, {"Friday", "online lessons"},
                        {"Saturday ", "help grandparents` garden"}});
        andresPet.respond();
        andresPet.eat();
        andresPet.foul();
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println(andresPet.toString());
        andres.describePet();
        andres.greetPet();
        System.out.println(andres.toString());
    }
}